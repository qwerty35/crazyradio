#include <Crazyradio.hpp>

Crazyradio::Crazyradio(const std::string link_uri, const std::string tf_prefix, const std::vector<std::string> topic_name, const std::vector<std::string> channel)
: USBDevice(0x1915, 0x7777), devId_(0), address_(0),
  datarate_(Crazyradio::Datarate_250KPS), mode_(MODE_PTX), tf_prefix_(tf_prefix), topic_name_(topic_name), channel_str_(channel) {
	topic_number_ = topic_name_.size();
	odometry_pub_.resize(topic_number_);
	odometry_sub_.resize(topic_number_);
	odometry_msg_.resize(topic_number_);
	transmittedTime_.resize(topic_number_);
	odometrySubFlag_.resize(topic_number_);
	channel_.resize(topic_number_);
	for(int i=0; i<topic_number_; i++)
	{
		channel_[i] = std::atoi (channel_str_[i].c_str());
		std::cout << (unsigned)channel_[i] << std::endl;
	}

	//url check
	int datarate;
	char datarateType;
	bool success = false;
	char mode = 0;
	
	success = std::sscanf(link_uri.c_str(), "radio://%d/%d%c/%c", &devId_, &datarate, &datarateType, &mode) == 4;
	address_ = 0xE7E7E7E7E7;
	if (success)
	{
		if (datarate == 250 && datarateType == 'K') {
			datarate_ = Crazyradio::Datarate_250KPS;
		}
		else if (datarate == 1 && datarateType == 'M') {
			datarate_ = Crazyradio::Datarate_1MPS;
		}
		else if (datarate == 2 && datarateType == 'M') {
			datarate_ = Crazyradio::Datarate_2MPS;
		}

		open(devId_);
		sendVendorSetup(SET_DATA_RATE, datarate_, 0, NULL, 0);
		sendVendorSetup(SET_RADIO_CHANNEL, channel_[0], 0, NULL, 0);
		sendVendorSetup(SET_CONT_CARRIER, false, 0, NULL, 0);
		setAddress(address_);
		sendVendorSetup(SET_RADIO_POWER, Power_0DBM, 0, NULL, 0);
		sendVendorSetup(SET_RADIO_ARC, 5, 0, NULL, 0);
		sendVendorSetup(SET_RADIO_ARD, 0x80 | 32, 0, NULL, 0);
		sendVendorSetup(ACK_ENABLE, true, 0, NULL, 0);

		if  (mode == 'T') {
			mode_ = Crazyradio::MODE_PTX;
			sendVendorSetup(SET_MODE, mode_, 0, NULL, 0);
		}
		else if (mode == 'R') {
			mode_ = Crazyradio::MODE_PRX;
			sendVendorSetup(SET_MODE, mode_, 0, NULL, 0);
		}
		
		if (devId_ >= MAX_RADIOS) {
			throw std::runtime_error("This version does not support that many radios. Adjust MAX_RADIOS and recompile!");
		}
	}
	
	if (!success) {
		throw std::runtime_error("Uri is not valid!");
	}

	t_init_ = ros::WallTime::now();
	thread_ = std::thread(&Crazyradio::run, this);
}

Crazyradio::~Crazyradio() {
}

void Crazyradio::run() {
	ros::NodeHandle nh;
	if(mode_ == Crazyradio::MODE_PTX) {
		for(int i=0; i<topic_number_; i++) {
			std::cout << "topic_name: " << topic_name_[i] << std::endl;
			odometry_sub_[i] = nh.subscribe<nav_msgs::Odometry>
					( topic_name_[i], 1,
					boost::bind(&Crazyradio::odometry_cb, this, _1, i),
					NULL,
					ros::TransportHints().tcpNoDelay() );
		}
	} else if(mode_ == Crazyradio::MODE_PRX) {
		for(int i=0; i<topic_number_; i++) {
			odometry_pub_[i] = nh.advertise<nav_msgs::Odometry>(topic_name_[i], 1);
		}
	}

	std::fill(transmittedTime_.begin(), transmittedTime_.end(), t_init_);

	double hertz = 100.0;
	ros::Rate rate(2.0*hertz*topic_number_);

	int loopCounter = 0;
	while(ros::ok()) {
		ros::spinOnce();
		if(mode_ == Crazyradio::MODE_PTX) {
			bool transmitted = false;
			static int pingRequest = 0;
			static int pingResult = 0;
			static double pingQuality = 0.0;

			for(int i=0; i<topic_number_; i++) {
				static ros::WallTime t_now;
				t_now = ros::WallTime::now();
				if(odometrySubFlag_[i] && ((t_now - transmittedTime_[i]).toSec() >= (0.5/hertz))) {
					pingResult += transmitOdometry(odometry_msg_[i], i);
					pingRequest++;
					transmittedTime_[i] = t_now;
					odometrySubFlag_[i] = false;
					transmitted = true;
					rate.sleep();
				}
			}

			if(!transmitted) {
				pingResult += sendPing();
				pingRequest++;
				rate.sleep();
			}


			if(pingRequest>100*topic_number_) {
				pingQuality = (double)pingResult/pingRequest;
				printf("Link quality: %2.2f\n", pingQuality);

				pingRequest = 0;
				pingResult = 0;
			}
		}
		else if(mode_ == Crazyradio::MODE_PRX) {
			Ack data;
			// uint8_t data_tx[32];
			// for(int i=0; i<32; i++) data_tx[i] = 32-i;
			receivePackets(data);
			// sendPacketNoAck(data_tx, 32);
			// std::cout << tf_prefix_ + "Packet Sent" << std::endl;
			rate.sleep();
		}
	}
}

void Crazyradio::stop() {
	thread_.join();
}

bool Crazyradio::transmitPackets(uint8_t* data, int length) {
	Crazyradio::Ack ack;
	sendPacket(data, length, ack);
	if(ack.ack) handleAck(ack);

	return ack.ack;
}

bool Crazyradio::receivePackets(Ack& result) {
	result.ack = false;
	result.size = 0;
	
	int status;
	int transferred;

	// Read result
	status = libusb_bulk_transfer(
	    m_handle,
	    /* endpoint*/ (0x81 | LIBUSB_ENDPOINT_IN),
	    (unsigned char*)&result,
	    sizeof(result) - 1,
	    &transferred,
	    /*timeout*/ 1000);
	if (status == LIBUSB_ERROR_TIMEOUT) {
	    return false;
	}
	if (status != LIBUSB_SUCCESS) {
	    throw std::runtime_error(libusb_error_name(status));
	}
	result.size = transferred-1;
	// std::cout << tf_prefix_ + "_ack: " << (unsigned)result.ack << std::endl;
	// std::cout << tf_prefix_ + "_powerDet: " << (unsigned)result.powerDet << std::endl;
	// std::cout << tf_prefix_ + "_retry: " << (unsigned)result.retry << std::endl;
	if(result.ack) {
		handleAck(result);
		result.data[1] = 0xFF;
		result.data[2] = 0xFF;
	}

	return result.ack;
}

bool Crazyradio::sendPing() {
	Crazyradio::Ack ack;
	uint8_t ping = 0xFF;
	sendPacket(&ping, sizeof(ping), ack);
	return ack.ack;
}

void Crazyradio::sendPacket(const uint8_t* data, uint32_t length, Ack& result) {
	result.ack = false;
	result.size = 0;
	
	int status;
	int transferred;
	
	if (!m_handle) {
	    throw std::runtime_error("No valid device handle!");
	}
	
	// Send data
	status = libusb_bulk_transfer(
	    m_handle,
	    /* endpoint*/ (0x01 | LIBUSB_ENDPOINT_OUT),
	    (uint8_t*)data,
	    length,
	    &transferred,
	    /*timeout*/ 1000);
	if (status == LIBUSB_ERROR_TIMEOUT) {
	    return;
	}
	if (status != LIBUSB_SUCCESS) {
	    throw std::runtime_error(libusb_error_name(status));
	}
	if (length != transferred) {
	    std::stringstream sstr;
	    sstr << "Did transfer " << transferred << " but " << length << " was requested!";
	    throw std::runtime_error(sstr.str());
	}
	
	// Read result
	status = libusb_bulk_transfer(
	    m_handle,
	    /* endpoint*/ (0x81 | LIBUSB_ENDPOINT_IN),
	    (unsigned char*)&result,
	    sizeof(result) - 1,
	    &transferred,
	    /*timeout*/ 1000);
	if (status == LIBUSB_ERROR_TIMEOUT) {
	    return;
	}
	if (status != LIBUSB_SUCCESS) {
	    throw std::runtime_error(libusb_error_name(status));
	}
	result.size = transferred - 1;
}

void Crazyradio::sendPacketNoAck(const uint8_t* data, uint32_t length) {
	int status;
	int transferred;
	
	if (!m_handle) {
	    throw std::runtime_error("No valid device handle!");
	}
	
	// Send data
	status = libusb_bulk_transfer(
	    m_handle,
	    /* endpoint*/ (0x01 | LIBUSB_ENDPOINT_OUT),
	    (uint8_t*)data,
	    length,
	    &transferred,
	    /*timeout*/ 1000);
	if (status == LIBUSB_ERROR_TIMEOUT) {
	    return;
	}
	if (status != LIBUSB_SUCCESS) {
	    throw std::runtime_error(libusb_error_name(status));
	}
	if (length != transferred) {
	    std::stringstream sstr;
	    sstr << "Did transfer " << transferred << " but " << length << " was requested!";
	    throw std::runtime_error(sstr.str());
	}
}

void Crazyradio::setAddress(uint64_t address) {
	unsigned char a[5];
	a[4] = (address >> 0) & 0xFF;
	a[3] = (address >> 8) & 0xFF;
	a[2] = (address >> 16) & 0xFF;
	a[1] = (address >> 24) & 0xFF;
	a[0] = (address >> 32) & 0xFF;
	
	// sendVendorSetup(SET_RADIO_ADDRESS, 0, 0, a, 5);
	// unsigned char a[] = {0xe7, 0xe7, 0xe7, 0xe7, 0x02};
	
	int status = libusb_control_transfer(
	    m_handle,
	    LIBUSB_REQUEST_TYPE_VENDOR,
	    SET_RADIO_ADDRESS,
	    0,
	    0,
	    a,
	    5,
	    /*timeout*/ 1000);
	// if (status != LIBUSB_SUCCESS) {
	//     std::cerr << "sendVendorSetup: " << libusb_error_name(status) << std::endl;
	// }
}

void Crazyradio::handleAck(const Crazyradio::Ack& result) {
	// std::cout << tf_prefix_ + ": "; 
	// for(int i=0; i<result.size; i++) printf("%d ", result.data[i]);
	// std::cout << std::endl;
	//Parse odometry message
	if(mode_ == MODE_PRX){
		// printf("%X\n", result.data[1]);
		if(result.data[0]==channel_[0]) {
			//for(int i=0; i<result.size; i++) printf("%X ", result.data[i]);
			//std::cout << std::endl;
			// std::cout << "2" << std::endl;
			uint16_t sum = 0;
			for(int i=3; i<31; i++) sum += result.data[i];
			// if((((sum & 0xF0) >> 8) == result.data[1]) && ((sum & 0x0F) == result.data[2])) {
			if((((sum >> 8) & 0xFF) == result.data[1]) && ((sum & 0xFF) == result.data[2])) {
				//int i = (unsigned)result.data[0];
				int i = 0;
				odometry_msg_[i].pose.pose.position.x = *((float*)(&result.data[3]));
				odometry_msg_[i].pose.pose.position.y = *((float*)(&result.data[7]));
				odometry_msg_[i].pose.pose.position.z = *((float*)(&result.data[11]));
				// revert quaternion using zero roll/pitch angles
				//odometry_msg_.pose.pose.orientation.z = *((float*)(&result.data[15]));
				double yaw = *((float*)(&result.data[15]));
				double cy = cos(yaw * 0.5);
				double sy = sin(yaw * 0.5);
				double cp = 1.0;
				double sp = 0.0;
				double cr = 1.0;
				double sr = 0.0;
				odometry_msg_[i].pose.pose.orientation.w = cy * cp * cr + sy * sp * sr;
				odometry_msg_[i].pose.pose.orientation.x = cy * cp * sr - sy * sp * cr;
				odometry_msg_[i].pose.pose.orientation.y = sy * cp * sr + cy * sp * cr;
				odometry_msg_[i].pose.pose.orientation.z = sy * cp * cr - cy * sp * sr;
				odometry_msg_[i].twist.twist.linear.x = *((float*)(&result.data[19]));
				odometry_msg_[i].twist.twist.linear.y = *((float*)(&result.data[23]));
				odometry_msg_[i].twist.twist.linear.z = *((float*)(&result.data[27]));
				odometry_pub_[i].publish(odometry_msg_[i]);
			}
		}
	}
}

void Crazyradio::odometry_cb(const nav_msgs::Odometry::ConstPtr &msg, const int id) {
	odometry_msg_[id] = *msg;
	odometrySubFlag_[id] = true;
}

bool Crazyradio::transmitOdometry(const nav_msgs::Odometry msg, const uint8_t id) {
	static dataConverter converter;

	sendVendorSetup(SET_RADIO_CHANNEL, channel_[id], 0, NULL, 0);
	converter.bytes[0] = 0b11111111; //0: ACK, 1: powerDet, 2..3: Reserved, 4..7: retry
	converter.bytes[1] = id; //Type
	converter.data[1] = msg.pose.pose.position.x;
	converter.data[2] = msg.pose.pose.position.y;
	converter.data[3] = msg.pose.pose.position.z;
	// compute yaw angle
	double w = msg.pose.pose.orientation.w;
	double x = msg.pose.pose.orientation.x;
	double y = msg.pose.pose.orientation.y;
	double z = msg.pose.pose.orientation.z;
	double siny_cosp = 2.0 * (w * z + x * y);
	double cosy_cosp = 1.0 - 2.0 * (y * y + z * z);
	converter.data[4] = std::atan2(siny_cosp, cosy_cosp);
	converter.data[5] = msg.twist.twist.linear.x;
	converter.data[6] = msg.twist.twist.linear.y;
	converter.data[7] = msg.twist.twist.linear.z;

	uint16_t sum = 0;
	for(int i=4; i<32; i++) {
		sum += converter.bytes[i];
	}
	converter.bytes[2] = (sum >> 8) & 0xFF; //Checksum1
	converter.bytes[3] = sum & 0xFF; //Checksum2
	return transmitPackets(converter.bytes, 32);
}
