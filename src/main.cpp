#include <ros/ros.h>
#include <Crazyradio.hpp>
#include <crazyradio/AddCrazyradio.h>
#include <crazyradio/RemoveCrazyradio.h>

std::map<std::string, Crazyradio*> crazyradios;
bool add_crazyradio(crazyradio::AddCrazyradio::Request  &req, crazyradio::AddCrazyradio::Response &res) {
	ROS_INFO("Adding %s as %s", req.uri.c_str(), req.tf_prefix.c_str());

	// Ignore if the uri is already in use
	if (crazyradios.find(req.uri) != crazyradios.end()) {
		ROS_ERROR("Cannot add %s, already added.", req.uri.c_str());
		return false;
	}

	Crazyradio* cr;
	cr = new Crazyradio(req.uri, req.tf_prefix, req.topic_name, req.channel);
	crazyradios[req.uri] = cr;

	return true;
}

bool remove_crazyradio(crazyradio::RemoveCrazyradio::Request  &req, crazyradio::RemoveCrazyradio::Response &res) {
	if (crazyradios.find(req.uri) == crazyradios.end()) {
		ROS_ERROR("Cannot remove %s, not connected.", req.uri.c_str());
		return false;
	}
	
	ROS_INFO("Removing crazyradio at uri %s.", req.uri.c_str());
	crazyradios[req.uri]->stop();
	delete crazyradios[req.uri];
	crazyradios.erase(req.uri);
	ROS_INFO("Crazyradio %s removed.", req.uri.c_str());
	return true;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "crazyradio");
	ros::NodeHandle nh;
	
	ros::ServiceServer serviceAdd = nh.advertiseService("crazyradio_add_rx/add_crazyradio", add_crazyradio);
	ros::ServiceServer serviceRemove = nh.advertiseService("remove_crazyradio", remove_crazyradio);

	ros::spin();
  return 0;
}
