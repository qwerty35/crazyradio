#include "ros/ros.h"
#include <crazyradio/AddCrazyradio.h>

int main(int argc, char **argv)
{
	ros::init(argc, argv, "crazyflie_add", ros::init_options::AnonymousName);
	ros::NodeHandle n("~");
	
	// read paramaters
	std::string uri;
	std::string tf_prefix;
    int topic_number;
	
	n.getParam("uri", uri);
	n.getParam("tf_prefix", tf_prefix);
	n.getParam("topic_number", topic_number);
	std::vector<std::string> topic_name(topic_number);
	std::vector<std::string> channel(topic_number);
	for(int i=0; i<topic_number; i++) {
		n.getParam("topic_name" + std::to_string(i+1), topic_name[i]); 
		n.getParam("channel" + std::to_string(i+1), channel[i]); 
		std::cout << "topic_name: " << topic_name[i] << std::endl;
		std::cout << "channel: " << channel[i] << std::endl;
	}
	
	ROS_INFO("wait_for_service /add_crazyradio");
	ros::ServiceClient addCrazyradioService = n.serviceClient<crazyradio::AddCrazyradio>("add_crazyradio");
	addCrazyradioService.waitForExistence();
	ROS_INFO("found /add_crazyradio");
	crazyradio::AddCrazyradio addCrazyradio;
	addCrazyradio.request.uri = uri;
	addCrazyradio.request.tf_prefix = tf_prefix;
	addCrazyradio.request.topic_name = topic_name;
	addCrazyradio.request.channel = channel;
	addCrazyradioService.call(addCrazyradio);
	
	return 0;
}
