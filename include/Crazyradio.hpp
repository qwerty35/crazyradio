#include <thread>

#include <ros/ros.h>
#include <inttypes.h>
#include <libusb-1.0/libusb.h>
#include <USBDevice.h>

#include <nav_msgs/Odometry.h>
#include <bitset>

#define MAX_RADIOS 16

enum
{
    SET_RADIO_CHANNEL   = 0x01,
    SET_RADIO_ADDRESS   = 0x02,
    SET_DATA_RATE       = 0x03,
    SET_RADIO_POWER     = 0x04,
    SET_RADIO_ARD       = 0x05,
    SET_RADIO_ARC       = 0x06,
    ACK_ENABLE          = 0x10,
    SET_CONT_CARRIER    = 0x20,
    SCANN_CHANNELS      = 0x21,
	SET_MODE            = 0x22,
    LAUNCH_BOOTLOADER   = 0xFF,
};

class Crazyradio 
: public USBDevice {
	public:
		struct Ack
		{
		  Ack()
		    : ack(0)
		    , size(0)
		  {}
		
		  uint8_t ack:1;
		  uint8_t powerDet:1;
		  uint8_t retry:4;
		  uint8_t data[32];
		
		  uint8_t size;
		}__attribute__((packed));

		enum Datarate {
			Datarate_250KPS = 0,
			Datarate_1MPS   = 1,
			Datarate_2MPS   = 2,
		};

		enum Power
		{
			Power_M18DBM = 0,
			Power_M12DBM = 1,
			Power_M6DBM  = 2,
			Power_0DBM   = 3,
		};

		enum Mode
		{
			MODE_PTX = 0,
			MODE_PRX = 2,
		};

		union dataConverter {
		float data[8];
		uint8_t bytes[32];
		};

		Crazyradio(const std::string, const std::string, const std::vector<std::string>, const std::vector<std::string>);
		~Crazyradio();

		void run();
		void stop();

	private:
		std::thread thread_;

		std::vector<std::string> topic_name_, channel_str_;
		int topic_number_;

		std::string tf_prefix_;
		std::vector<uint8_t> channel_;
		uint64_t address_;
		Datarate datarate_;	
		int mode_;
		int devId_;

		void setAddress(uint64_t address);

		void handleAck(const Crazyradio::Ack& result);

		bool transmitPackets(uint8_t* data, int length);
		bool receivePackets(Ack& result);
		bool sendPing();
		void sendPacket(const uint8_t* data, uint32_t length, Ack& result);
		void sendPacketNoAck(const uint8_t* data, uint32_t length);
		bool transmitOdometry(const nav_msgs::Odometry, const uint8_t id);

		std::vector<ros::Publisher> odometry_pub_;
		std::vector<ros::Subscriber> odometry_sub_;
		void odometry_cb(const nav_msgs::Odometry::ConstPtr &, const int id);
		std::vector<nav_msgs::Odometry> odometry_msg_;

		ros::WallTime t_init_;
		std::vector<ros::WallTime> transmittedTime_;
		std::vector<bool> odometrySubFlag_;
};
